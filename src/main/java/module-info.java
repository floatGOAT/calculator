module ru.dryus.calcualtor {
    requires javafx.controls;
    requires javafx.fxml;


    opens ru.dryus.calcualtor to javafx.fxml;
    exports ru.dryus.calcualtor;
}